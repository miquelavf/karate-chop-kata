using System;
using FluentAssertions;
using KarateChop;
using Xunit;

namespace Test
{
    public class ChopperShould
    {
        [Fact]
        public void Return_negative_one_when_value_is_not_found()
        {
            var givenValue = 1;
            var givenArray = new int[] { };
            var karateChopper = new KarateChopper();
            var expectedIndex = -1;

            var actualIndex = karateChopper.Search(givenValue, givenArray);

            actualIndex.Should().Be(expectedIndex);
        }

        [Fact]
        public void Return_the_items_index_on_single_item_array()
        {
            var givenValue = 1;
            var givenArray = new int[] { 1 };
            var karateChopper = new KarateChopper();
            var expectedIndex = 0;

            var actualIndex = karateChopper.Search(givenValue, givenArray);

            actualIndex.Should().Be(expectedIndex);
        }
        
        [Fact]
        public void Return_the_items_index_on_two_item_array()
        {
            var givenValue = 1;
            var givenArray = new int[] { 1, 2 };
            var karateChopper = new KarateChopper();
            var expectedIndex = 0;

            var actualIndex = karateChopper.Search(givenValue, givenArray);

            actualIndex.Should().Be(expectedIndex);
        }

        [Fact]
        public void Return_the_items_index_on_eleven_item_array_last()
        {
            var givenValue = 11;
            var givenArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
            var karateChopper = new KarateChopper();
            var expectedIndex = 10;

            var actualIndex = karateChopper.Search(givenValue, givenArray);

            actualIndex.Should().Be(expectedIndex);
        }

        [Fact]
        public void Return_the_items_index_on_ten_item_array_lower_half()
        {
            var givenValue = 4;
            var givenArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var karateChopper = new KarateChopper();
            var expectedIndex = 3;

            var actualIndex = karateChopper.Search(givenValue, givenArray);

            actualIndex.Should().Be(expectedIndex);
        }
    }
}
