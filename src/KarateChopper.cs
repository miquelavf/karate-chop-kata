using System;
using System.Collections;
using System.Linq;

namespace KarateChop
{
    public class KarateChopper
    {
        private const sbyte NotFound = -1;

        public int Search(int value, int[] values, int index = 0)
        {
            if (HasValues(values))
            {
                if (IsLastResult(values))
                {
                    return IsCorrectResult(value, values[0], index);
                }

                var newValues = TakeHalf(value, values, index);

                return Search(value, newValues.values, newValues.index);
            }

            return NotFound;
        }

        private bool HasValues(int[] values) => values.Length > 0;
        private bool IsLastResult(int[] values) => values.Length == 1;
        private int IsCorrectResult(int expectedValue, int actualValue, int index) =>
            actualValue == expectedValue ? index : NotFound;
        private (int[] values, int index) TakeHalf(int value, int[] values, int index)
        {
            var halfLength = GetHalfLength();

            return values[halfLength] <= value ? 
                (values.Skip(halfLength).ToArray(), index + halfLength) :
                (values.Take(halfLength).ToArray(), index);

            int GetHalfLength() => values.Length % 2 == 0 ? 
                values.Length / 2 :
                (values.Length / 2) + (values.Length % 2);
        }
    }
}