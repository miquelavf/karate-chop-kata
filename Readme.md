# Karate Chop Kata

This is an implementation on the [_Karate Chop Kata_](http://codekata.com/kata/kata02-karate-chop/) from [codekata.com](http://codekata.com/) in C# using [_TDD_](https://en.wikipedia.org/wiki/Test-driven_development).